#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 检测域名错误状态
# 
import requests
import re


def main():
    url_list = ['www.baidu.com', 'www.xxxdfadfaefdefaefaefaefe.com', 'www.10086.cn']
    for url in url_list:
        rs = check_url(url)
        print rs
        # params_request(url)


def check_url(url):
    url = format_url(url)

    # rs = requests.get(url, timeout=3, verify=False)

    try:
        rs = requests.get(url, timeout=3, verify=False)
    except requests.exceptions.ConnectionError as e:
        print str(e.message)

    # print(rs.status_code)
    # print(rs.c)


def format_url(str):
    # rs = re.match('((xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}', tmp)

    url=str.strip()
    if re.match(r'^https?:/{2}\w.+$', url):
        return url
    else:
        return 'http://'+url


def params_request(url):
    # 通过exceptions异常来判断请求是否成功
    try:
        t1 = time.time()
        response = requests.get(url,timeout=0.1)
        t2 = time.time()
    except exceptions.Timeout as e:
        print('请求超时：'+str(e.message))
    except exceptions.HTTPError as e:
        print('http请求错误:'+str(e.message))
    else:
        # 通过status_code判断请求结果是否正确
        print('请求耗时%ss'%(t2-t1))
        if response.status_code == 200:
            print(better_print(response.text))
            print(response.request.headers)
            print(str(response.status_code),response.url)
        else:
            print('请求错误：'+str(response.status_code)+','+str(response.reason))


if __name__ == "__main__":
    main()
