# linux 常见条件判断 & 参数获取

## 一、获取参数

### 1.1 判断centos版本
```bash
CentOS_Version=`rpm --eval '%{centos_ver}'`
if [ ${CentOS_Version} -eq 7 ]; then
	echo 'System check ok'
else
	echo "Only can use for CentOS7"
fi

```


## 二、常用判断

### 2.1 判断文件中是否有特定字符串

```bash
if ! cat ~/.ssh/config |grep "HostName github.com" >/dev/null 2>&1; then echo "ok"; fi
```


### 2.2 判断命令是否存在

```bash
if command -v get.ip >/dev/null 2>&1; then
  echo 'exists git'
else
  echo 'no exists git'
fi

```
