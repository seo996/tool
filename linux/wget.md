# wget 常用

```bash
wget -r -U "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36" http://www.cnautonews.com/

wget -e robots=off -k -r -U "Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)" http://www.cnautonews.com/

```

```
-U 指定UA
-e robots=off 忽略robots限制


```
