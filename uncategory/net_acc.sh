#!/bin/bash

function hello() {
  echo ""
  echo -e "\033[33mUbuntu & Debian 網路優化腳本\033[0m"
  echo -e "\033[33m訪問 https://ralf.ren 獲取更多資訊。\033[0m"
  echo ""

  if [ $UID -ne 0 ]; then
    echo -e "\033[41;37m需要超級用戶權限來執行該腳本。 \033[0m"
    echo -e "\033[41;37m請使用 \"sudo bash $0\" \033[0m"
    exit 100
  fi
}

function auto() {
  ARCH="64"
  KERN="4.9.0"
  KERM="4"
  KERS="9"
  # Debian 7
  if [[ -n "`cat /etc/issue | grep "Debian.*7"`" ]]; then
    [ -n "`uname -a | grep "x86_64"`" ] && ARCH=64
    [ -n "`uname -a | grep "i386"`" ] && ARCH=32
    [ -n "`uname -a | grep "i686"`" ] && ARCH=32
    echo "您正在使用 $ARCH位元 的 Debian 7。"
    echo "偏好網路加速方案爲銳速。"
    KERN=`uname -r | grep -o "^[0-9]\.[0-9]*\.[0-9]*"`
    wget https://raw.githubusercontent.com/0oVicero0/serverSpeeder_kernel/master/serverSpeeder.txt >/dev/null 2>&1
    if [[ -n "`cat serverSpeeder.txt | grep "Debian/7/$KERN.*/x$ARCH"`" ]]; then
      echo "內核 $KERN 受銳速支援。"
      rm -f serverSpeeder.txt
      echo "下載銳速安裝腳本..."
      wget --no-check-certificate "https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh" >/dev/null 2>&1
      echo "按下回車鍵啓動銳速安裝。"
      read
      bash appex.sh install
      clear
      echo "正在驗證安裝..."
      if [[ -f /etc/init.d/serverSpeeder ]]; then
        echo "找到 /etc/init.d/serverSpeeder"
        echo "執行清理..."
        rm -f appex.sh >/dev/null 2>&1
        echo "完成。"
      else
        echo -e "\033[41;37m在安裝銳速時出現問題。\033[0m"
        exit 3
      fi
    else
      echo "內核 $KERN 不受銳速支援。"
      echo "準備安裝新內核..."
      apt-get update >/dev/null 2>&1
      echo "安裝新內核..."
      apt-get install -y linux-image-3.2.0-4* >/dev/null 2>&1
      echo "配置GRUB..."
      update-grub >/dev/null 2>&1
      echo ""
      echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
      echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
    fi
  # Debian 8
  elif [[ -n "`cat /etc/issue | grep "Debian.*8"`" ]]; then
    [ -n "`uname -a | grep "x86_64"`" ] && ARCH=64
    [ -n "`uname -a | grep "i386"`" ] && ARCH=32
    [ -n "`uname -a | grep "i686"`" ] && ARCH=32
    KERN=`uname -r | grep -o "^[0-9]\.[0-9]*\.[0-9]*-[0-9]*"`
    KERM=`uname -r | grep -o "^[0-9]"`
    KERS=`uname -r | grep -o "\..*\." | grep -o "[0-9]*"`
    if [[ $ARCH -eq 32 ]]; then
      echo "您正在使用 32位元 的 Debian 8。"
      echo "偏好網路加速方案爲BBR。"
      if [ $KERM -eq 4 ] || [ $KERS -ge 9 ]; then
        echo "內核 $KERN 受BBR支援。"
        echo "啓用BBR..."
        echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
        echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
        sysctl -p >/dev/null 2>&1
        echo "完成。"
        echo ""
        echo -e "\033[33mBBR模組已啓用：\033[0m"
        echo `lsmod | grep bbr`
      else
        echo "內核 $KERN 不受BBR支援。"
        echo "準備安裝新內核..."
        wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.9.6/linux-image-4.9.6-040906-generic_4.9.6-040906.201701260330_i386.deb >/dev/null 2>&1
        echo "安裝新內核..."
        dpkg -i linux-image-4.9.6-040906-generic_4.9.6-040906.201701260330_i386.deb >/dev/null 2>&1
        echo "配置GRUB..."
        update-grub >/dev/null 2>&1
        echo ""
        echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
        echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
      fi
    elif [[ $ARCH -eq 64 ]]; then
      echo "您正在使用 64位元 的 Debian 8。"
      if [[ $KERM -eq 3 ]]; then
        echo "偏好網路加速方案爲銳速。"
        if [[ $KERN == "3.16.0-4" ]]; then
          echo "內核 $KERN 受銳速支援。"
          echo "下載銳速安裝腳本..."
          wget --no-check-certificate "https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh" >/dev/null 2>&1
          echo "按下回車鍵啓動銳速安裝。"
          read
          bash appex.sh install
          clear
          echo "正在驗證安裝..."
          if [[ -f /etc/init.d/serverSpeeder ]]; then
            echo "找到 /etc/init.d/serverSpeeder"
            echo "執行清理..."
            rm -f appex.sh >/dev/null 2>&1
            echo "完成。"
          else
            echo -e "\033[41;37m在安裝銳速時出現問題。\033[0m"
            exit 3
          fi
        else
          echo "內核 $KERN 不受銳速支援。"
          echo "準備安裝新內核..."
          apt-get update >/dev/null 2>&1
          echo "安裝新內核..."
          apt-get install -y linux-image-3.16.0-4* >/dev/null 2>&1
          echo "配置GRUB..."
          update-grub >/dev/null 2>&1
          echo ""
          echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
          echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
        fi
      elif [[ $KERM -eq 4 ]]; then
        echo "偏好網路加速方案爲 BBR。"
        if [ $KERS -ge 9 ]; then
          echo "內核 $KERN 受BBR支援。"
          echo "啓用BBR..."
          echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
          echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
          sysctl -p >/dev/null 2>&1
          echo "完成。"
          echo ""
          echo -e "\033[33mBBR模組已啓用：\033[0m"
          echo `lsmod | grep bbr`
        else
          echo "內核 $KERN 不受BBR支援。"
          echo "準備安裝新內核..."
          wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.9.6/linux-image-4.9.6-040906-generic_4.9.6-040906.201701260330_amd64.deb >/dev/null 2>&1
          echo "安裝新內核..."
          dpkg -i linux-image-4.9.6-040906-generic_4.9.6-040906.201701260330_amd64.deb >/dev/null 2>&1
          echo "配置GRUB..."
          update-grub >/dev/null 2>&1
          echo ""
          echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
          echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
        fi
      fi
    else
      echo -e "\033[41;37m您的Linux發行版不受支援。\033[0m"
      exit 1
    fi
  # Debian 9
  elif [[ -n "`cat /etc/issue | grep "Debian.*9"`" ]]; then
    [ -n "`uname -a | grep "x86_64"`" ] && ARCH=64
    [ -n "`uname -a | grep "i386"`" ] && ARCH=32
    [ -n "`uname -a | grep "i686"`" ] && ARCH=32
    echo "您正在使用 $ARCH位元 的 Debian 9。"
    echo "偏好網路加速方案爲 BBR。"
    KERN=`uname -r | grep -o "^[0-9]\.[0-9]*\.[0-9]*-[0-9]*"`
    KERM=`uname -r | grep -o "^[0-9]"`
    KERS=`uname -r | grep -o "\..*\." | grep -o "[0-9]*"`
    if [ $KERM -eq 4 ] || [ $KERS -ge 9 ]; then
      echo "內核 $KERN 受BBR支援。"
      echo "啓用BBR..."
      echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
      echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
      sysctl -p >/dev/null 2>&1
      echo "完成。"
      echo ""
      echo -e "\033[33mBBR模組已啓用：\033[0m"
      echo `lsmod | grep bbr`
    else
      echo ""
      echo -e "\033[41;37m您正在使用非標準的發行版。\033[0m"
      exit 2
    fi
  # Ubuntu 14.04
  elif [[ -n "`cat /etc/issue | grep "Ubuntu 14.04"`" ]]; then
    [ -n "`uname -a | grep "x86_64"`" ] && ARCH=64
    [ -n "`uname -a | grep "i386"`" ] && ARCH=32
    [ -n "`uname -a | grep "i686"`" ] && ARCH=32
    echo "您正在使用 $ARCH位元 的 Ubuntu 14.04。"
    echo "偏好網路加速方案爲銳速。"
    KERN=`uname -r | grep -o "^[0-9]\.[0-9]*\.[0-9]*-[0-9]*"`
    wget https://raw.githubusercontent.com/0oVicero0/serverSpeeder_kernel/master/serverSpeeder.txt >/dev/null 2>&1
    if [[ -n "`cat serverSpeeder.txt | grep "Ubuntu/14.04/$KERN.*/x$ARCH"`" ]]; then
      echo "內核 $KERN 受銳速支援。"
      rm -f serverSpeeder.txt
      echo "下載銳速安裝腳本..."
      wget --no-check-certificate "https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh" >/dev/null 2>&1
      echo "按下回車鍵啓動銳速安裝。"
      read
      bash appex.sh install
      clear
      echo "正在驗證安裝..."
      if [[ -f /etc/init.d/serverSpeeder ]]; then
        echo "找到 /etc/init.d/serverSpeeder"
        echo "執行清理..."
        rm -f appex.sh >/dev/null 2>&1
        echo "完成。"
      else
        echo -e "\033[41;37m在安裝銳速時出現問題。\033[0m"
        exit 3
      fi
    else
      echo "內核 $KERN 不受銳速支援。"
      echo "配置GRUB..."
      cp /etc/default/grub /etc/default/grub.old >/dev/null 2>&1
      sed -ir "s/GRUB_DEFAULT=.*/GRUB_DEFAULT=\"Advanced options for Ubuntu>Ubuntu, with Linux 3.13.0-29-generic\"/g" /etc/default/grub
      echo "準備安裝新內核..."
      apt-get update >/dev/null 2>&1
      echo "安裝新內核..."
      apt-get install -y linux-image-extra-3.13.0-29-generic >/dev/null 2>&1
      echo ""
      echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
      echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
    fi
  # Ubuntu 16.04
  elif [[ -n "`cat /etc/issue | grep "Ubuntu 16.04"`" ]]; then
    [ -n "`uname -a | grep "x86_64"`" ] && ARCH=64
    [ -n "`uname -a | grep "i386"`" ] && ARCH=32
    [ -n "`uname -a | grep "i686"`" ] && ARCH=32
    if [[ $ARCH -eq 64 ]]; then
      echo "您正在使用 64位元 的 Ubuntu 16.04。"
      echo "偏好網路加速方案爲銳速。"
      KERN=`uname -r | grep -o "^[0-9]\.[0-9]*\.[0-9]*-[0-9]*"`
      if [[ $KERN == "4.4.0-47" ]]; then
        echo "內核 $KERN 受銳速支援。"
        echo "下載銳速安裝腳本..."
        wget --no-check-certificate "https://raw.githubusercontent.com/0oVicero0/serverSpeeder_Install/master/appex.sh" >/dev/null 2>&1
        echo "按下回車鍵啓動銳速安裝。"
        read
        bash appex.sh install
        clear
        echo "正在驗證安裝..."
        if [[ -f /etc/init.d/serverSpeeder ]]; then
          echo "找到 /etc/init.d/serverSpeeder"
          echo "執行清理..."
          rm -f appex.sh >/dev/null 2>&1
          echo "完成。"
        else
          echo -e "\033[41;37m在安裝銳速時出現問題。\033[0m"
          exit 3
        fi
      else
        echo "內核 $KERN 不受銳速支援。"
        echo "配置GRUB..."
        cp /etc/default/grub /etc/default/grub.old >/dev/null 2>&1
        sed -ir "s/GRUB_DEFAULT=.*/GRUB_DEFAULT=\"Advanced options for Ubuntu>Ubuntu, with Linux 4.4.0-47-generic\"/g" /etc/default/grub
        echo "準備安裝新內核..."
        apt-get update >/dev/null 2>&1
        echo "安裝新內核..."
        apt-get install -y linux-image-extra-4.4.0-47-generic >/dev/null 2>&1
        echo ""
        echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
        echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
      fi
    elif [[ $ARCH -eq 32 ]]; then
      echo "您正在使用 32位元 的 Ubuntu 16.04。"
      echo "偏好網路加速方案爲 BBR。"
      KERM=`uname -r | grep -o "^[0-9]"`
      KERS=`uname -r | grep -o "\..*\." | grep -o "[0-9]*"`
      KERN=`uname -r | grep -o "^[0-9]\.[0-9]*\.[0-9]*-[0-9]*"`
      if [ $KERM -eq 4 ] || [ $KERS -ge 9 ]; then
        echo "內核 $KERN 受BBR支援。"
        echo "啓用BBR..."
        echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
        echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
        sysctl -p >/dev/null 2>&1
        echo "完成。"
        echo ""
        echo -e "\033[33mBBR模組已啓用：\033[0m"
        echo `lsmod | grep bbr`
      else
        echo "內核 $KERN 不受BBR支援。"
        echo "準備安裝新內核..."
        wget http://kernel.ubuntu.com/~kernel-ppa/mainline/v4.9.6/linux-image-4.9.6-040906-generic_4.9.6-040906.201701260330_amd64.deb >/dev/null 2>&1
        echo "安裝新內核..."
        dpkg -i linux-image-4.9.6-040906-generic_4.9.6-040906.201701260330_amd64.deb >/dev/null 2>&1
        echo "配置GRUB..."
        update-grub >/dev/null 2>&1
        echo ""
        echo -e "\033[33m已更換爲支援銳速的內核，需要重新啓動。\033[0m"
        echo -e "\033[33m重新啓動後，請使用 \"bash $0\" 命令再次執行腳本來完成安裝。\033[0m"
      fi
    fi
  # Unsupported distribution
  else
    echo -e "\033[41;37m您的Linux發行版不受支援。\033[0m"
    exit 1
  fi
}

hello;
auto;
