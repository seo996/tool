@echo off
:: 开启延迟变量 解决if语句中set变量异常
setlocal EnableDelayedExpansion
REM @chcp 437
CURDIR=%cd%
:: 获取admin权限
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
if '%errorlevel%' NEQ '0' (
goto UACPrompt
) else ( goto gotAdmin )
:UACPrompt
echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
"%temp%\getadmin.vbs"
exit /B
:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )

:: 设置初始变量
REM set DownHost1=https://xxfile.obs.ap-southeast-1.myhuaweicloud.com/down
set DownHost1=https://xxx/down
set MainHost=47.xxx.xxx


:: 创建工作目录
md "C:\workspace"
md "C:\workspace\Downloads"
set DirWork=C:\workspace
set DirDownload=C:\workspace\Downloads
set Save=%DirDownload%
REM if exist %Save% (echo Save Path：%Save%) else (mkdir %Save% & echo Created Dir OK：%Save%)
REM if not defined Save set "Save=%cd%"
(echo Download Wscript.Arguments^(0^),Wscript.Arguments^(1^)
echo Sub Download^(url,target^)
echo   Const adTypeBinary = 1
echo   Const adSaveCreateOverWrite = 2
echo   Dim http,ado
echo   Set http = CreateObject^("Msxml2.ServerXMLHTTP"^)
echo   http.open "GET",url,False
echo   http.send
echo   Set ado = createobject^("Adodb.Stream"^)
echo   ado.Type = adTypeBinary
echo   ado.Open
echo   ado.Write http.responseBody
echo   ado.SaveToFile target
echo   ado.Close
echo End Sub)>DownloadFile.vbs

reg query HKEY_CURRENT_USER\Software\Google\Chrome\BLBeacon\|find /i "version">nul 2>nul
if %errorlevel%==0 ( echo "[check] GoogleChrome：Installed" ) else (
    echo "[check] GoogleChrome：not install"
    set DownTpUrl=%DownHost1%/ChromeSetup.exe
    for %%a in ("!DownTpUrl!") do set "DownTpName=%%~nxa"
    echo "Downloading !DownTpName! from !DownTpUrl!..."

    if not exist %DirDownload%\!DownTpName! ( DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" )
    REM if not exist %DirDownload%\!DownTpName! (
    REM     echo !DownTpName! download error, try another link
    REM     set DownTpUrl=%DownHost2%/ChromeSetup.exe
    REM     DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" 
    REM )
    start /WAIT %DirDownload%\!DownTpName! 
    reg query HKEY_CURRENT_USER\Software\Google\Chrome\BLBeacon\|find /i "version">nul 2>nul
    if !errorlevel!==0 ( echo "[ok] GoogleChrome Installed" ) else ( echo "[error] GoogleChrome Install Error.." )
)
echo.

:: code install
set TpName=VSCode
set BIN_CODE="C:\Program Files\Microsoft VS Code\Code.exe"
echo "Checking %TpName%..."
if exist %BIN_CODE% ( echo "[check] %TpName%：Installed" ) else (
    set DownTpUrl=%DownHost1%/VSCodeSetup-x64-1.40.1.exe
    echo "[check] %TpName%: Not Install"
    for %%a in ("!DownTpUrl!") do set "DownTpName=%%~nxa"
    if not exist %DirDownload%\!DownTpName! ( DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" )

    REM if not exist %DirDownload%\!DownTpName! (
    REM     echo !DownTpName! download error, try another link
    REM     set DownTpUrl=https://pubfile.xxooxx.ga/source/code-server/VSCodeSetup-x64-1.40.1.exe
    REM     DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" 
    REM )
    
    start /WAIT %DirDownload%\!DownTpName!
    if exist %BIN_CODE% ( echo "[ok] %TpName% Installed" ) else ( echo "[error] %TpName% Install Error" )
)

:: winrar install
set TpName=WinRAR
SET BIN_RAR="C:\Program Files\WinRAR\WinRAR.exe"
echo "Checking %TpName%.."
if not exist %BIN_RAR% (
    echo "[check] WinRAR: not install"
    set UrlWinrar=%DownHost1%/winrar-x64-571tc.exe
    set UrlWinrarKey=%DownHost1%/rarreg.key

    for %%a in ("!UrlWinrar!") do set "FileNameWinrar=%%~nxa"
    if not exist %DirDownload%\!FileNameWinrar! ( DownloadFile.vbs "!UrlWinrar!" "%DirDownload%\!FileNameWinrar!" )
    for %%a in ("!UrlWinrarKey!") do set "FileNameWinrarKey=%%~nxa"
    if not exist %DirDownload%\!FileNameWinrarKey! ( DownloadFile.vbs "!UrlWinrarKey!" "%DirDownload%\!FileNameWinrarKey!" )

    start /WAIT %DirDownload%\!FileNameWinrar!
    copy /y /r %DirDownload%\!FileNameWinrarKey!\ "C:\Program Files\WinRAR\" 
) else (
    echo "[check] Winrar already installed"
)

:: cmder install
if not exist "C:\workspace\soft\cmder\Cmder.exe" (
    set DownTpUrl=%DownHost1%/cmder.7z
    for %%a in ("!DownTpUrl!") do set "DownTpName=%%~nxa"
    if not exist %DirDownload%\!DownTpName! ( DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" )
    if not exist %DirDownload%\!DownTpName! ( 
        echo !DownTpName! download error, try another link
        set DownTpUrl=https://github.com/cmderdev/cmder/releases/download/v1.3.12/cmder.7z
        DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!"
    )
    %BIN_RAR% x "%DirDownload%\!DownTpName!" C:\workspace\soft\cmder\

) else (
    echo "[check] cmder already installed"
)
if not exist "C:\Users\Public\Desktop\Cmder" (
    if exist "C:\workspace\soft\cmder\Cmder.exe" ( mklink "C:\Users\Public\Desktop\Cmder" "C:\workspace\soft\cmder\Cmder.exe" )
)

:: python2 install
set TpName=Python2
if not exist "C:\Python27\python.exe" (
    echo "%TpName% not installed, downloading"
    set DownTpUrl=%DownHost1%/python-2.7.17.amd64.msi
    for %%a in ("!DownTpUrl!") do set "DownTpName=%%~nxa"
    if not exist %DirDownload%\!DownTpName! ( DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" )
    start /WAIT %DirDownload%\!DownTpName!
) else (
    echo "[check] Python2 already installed"
)

:: pip update
set TpName="PIP19"
set PATH=C:\Python27\;C:\Python27\Scripts;%PATH%
set BIN_PIP="C:\Python27\Scripts\pip.exe"
if not exist "C:\Python27\python.exe" (
    echo "Python2 Install Error.."
    pause
    exit /B
)
if not exist "C:\Python27\Scripts\pip-19.3.1\setup.py" ( 
    echo "[check] %TpName%：Not Install"
    set DownTpUrl=%DownHost1%/pip-19.3.1.tar.gz
    set DownTpName=pip-19.3.1.tar.gz
    if not exist %DirDownload%\!DownTpName! ( DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" )

    %BIN_RAR% x "%DirDownload%\!DownTpName!" C:\Python27\Scripts\
    cd C:\Python27\Scripts\
    del *.exe
    cd C:\Python27\Scripts\pip-19.3.1\
    python setup.py build
    python setup.py install
    if exist %BIN_PIP% ( echo "[ok] %TpName% Installed" ) else ( echo "[error] %TpName% Install Error!" )
    cd %CURDIR%

) else ( echo "[check] %TpName% already installed" )

:: pip 源： 
:: -i https://mirrors.ustc.edu.cn/pypi/web/simple
:: -i https://pypi.douban.com/simple
REM pip install -i https://mirrors.ustc.edu.cn/pypi/web/simple pip==10.0.1 -U
REM python -m pip install -U pip

if exist "C:\Python27\python.exe" if exist "C:\Python27\Scripts\pip-19.3.1\setup.py" (
    echo "Python2 and PIP19 install ok.."
) else (
    echo "Python2 and PIP19 install error.."
    pause
    exit /B
)

:: 安装pip依赖包
ping -n 2 www.google.com>nul 2>nul
if %errorlevel%==0 ( 
    set PIP_IMAGE=
) else (
    set PIP_IMAGE=-i https://mirrors.ustc.edu.cn/pypi/web/simple
)
pip install %PIP_IMAGE% requests wget selenium configparser simplejson progressbar pywin32
set TpName=PipPycurl
pip list|findstr "pycurl">nul 2>nul
if %errorlevel%==0 ( echo "[check] %TpName%：Installed" ) else (
    echo "[check] %TpName%: Not Install"
    :: from:https://www.lfd.uci.edu/~gohlke/pythonlibs/
    pip install %DownHost1%/pycurl-7.43.0.3-cp27-cp27m-win_amd64.whl
    pip list|findstr "pycurl">nul 2>nul
    if %errorlevel%==0 ( echo "[ok] %TpName% Installed" ) else ( echo "[error] %TpName% Install Error" )
)

:: nmap install
set TpName=nmap
set BIN_NMAP="C:\Program Files (x86)\Nmap\nmap.exe"
echo %BIN_NMAP%
if not exist %BIN_NMAP% (
    echo "[check] %TpName% Not Install, downloading..."
    set DownTpUrl=%DownHost1%/nmap-7.80-setup.exe
    for %%a in ("!DownTpUrl!") do set "DownTpName=%%~nxa"
    if not exist %DirDownload%\!DownTpName! ( DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!" )
    if not exist %DirDownload%\!DownTpName! ( 
        echo !DownTpName! download error, try another link
        set DownTpUrl=https://nmap.org/dist/nmap-7.80-setup.exe
        DownloadFile.vbs "!DownTpUrl!" "%DirDownload%\!DownTpName!"
    )
    start /WAIT %DirDownload%\!DownTpName!
) else (
    echo "[check] %TpName% already installed"
)

:: 添加host
REM :: ## install telnet
REM echo "[install] telnet"
REM dism /online /Enable-Feature /FeatureName:TelnetClient
if not exist %BIN_NMAP% (
    echo "NMAP Install Error.."
    pause
    exit /B
)
REM set hostspath=%SystemRoot%\System32\drivers\etc\hosts
REM %BIN_NMAP% -sT -p 2087 main.xxoo|findstr "open">nul 2>nul
REM if %errorlevel%==0 ( echo "[check] main hosts port check ok" ) else (
REM     echo "[check] main hosts port check error, try add hosts now.."
REM     attrib -R %hostspath%
REM     echo %MainHost% main.xxoo>>%hostspath%
REM     attrib +R %hostspath%
REM     %BIN_NMAP% -sT -p 2087 main.xxoo|findstr "open">nul 2>nul
REM     if !errorlevel!==0 ( echo "[ok] main hosts port check ok" ) else ( echo "[error] main hosts port add failure" )
REM )

::下载完删除生成的vbs文件和临时文件
del DownloadFile.vbs
pause